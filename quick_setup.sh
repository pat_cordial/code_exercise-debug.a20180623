#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
set -o

#build backend
php composer.phar install

#run migrations
php artisan migrate


#register some seed users
curl -vX POST "http://localhost/register" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"username\": \"jane\",  \"email\": \"janedoe@example.com\",  \"password\": \"test1234\"}"
curl -vX POST "http://localhost/register" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"username\": \"otherguy\",  \"email\": \"other.guy@example.com\",  \"password\": \"test5678\"}"
curl -vX POST "http://localhost/register" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"username\": \"tester1\",  \"email\": \"teter1@test.example.com\",  \"password\": \"test1234\"}"
curl -vX POST "http://localhost/register" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"username\": \"bob\",  \"email\": \"bob@example.com\",  \"password\": \"test5678\"}"
curl -vX POST "http://localhost/register" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"username\": \"John\",  \"email\": \"john-doe@example.com\",  \"password\": \"test5678\"}"

#create an api_token for John and Jane
curl -vX POST "http://localhost/login" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"email\": \"janedoe@example.com\",  \"password\": \"test1234\"}"
curl -vX POST "http://localhost/login" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"email\": \"john-doe@example.com\",  \"password\": \"test5678\"}"
